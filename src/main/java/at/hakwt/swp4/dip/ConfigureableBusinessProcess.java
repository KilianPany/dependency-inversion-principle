package at.hakwt.swp4.dip;

import java.util.ArrayList;
import java.util.List;

/**
 * A configureable implementation of a {@link BusinessProcess}. The
 * {@link #configure(List)} method must be initially called to set
 * the steps for this process.
 */
public class ConfigureableBusinessProcess implements BusinessProcess {

    private List<BusinessProcessStep> steps;
    public String chapterName;

    public ConfigureableBusinessProcess(String chapterName) {

        this.steps = new ArrayList<>();
        this.chapterName = chapterName;

    }

    public ConfigureableBusinessProcess(List<BusinessProcessStep> steps) {

        this.steps = new ArrayList<>();
        configure(steps);
    }

    public void configure(List<BusinessProcessStep> services) {

        this.steps.addAll(services);
    }

    public void run() {

        System.out.println(chapterName);

        for(BusinessProcessStep step : steps) {
            step.process();
        }
    }
}