package at.hakwt.swp4.dip.springframework;

public class FacebookSender implements SocialMediaSender{

    public void sendMessage(String message) {

        System.out.println("send message: " + message + " to Facebook");
    }
}