package at.hakwt.swp4.dip.springframework;

public class TwitterSender implements SocialMediaSender{

    public void sendMessage(String message) {

        System.out.println("send message: " + message + " to Twitter");
    }
}