package at.hakwt.swp4.dip.springframework;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SocialMediaMain {

    public static void main(String[] args) {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("socialMediaContacts.xml");

        SocialMediaManager manager = applicationContext.getBean("socialMediaManager", SocialMediaManager.class);

        manager.send("'Hi, how are you?'");
    }
}